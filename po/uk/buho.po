# Translation of buho.po to Ukrainian
# Copyright (C) 2020-2021 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: buho\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-22 00:43+0000\n"
"PO-Revision-Date: 2022-08-14 08:31+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Qt-Contexts: true\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: src/main.cpp:55 src/main.qml:16
#, kde-format
msgid "Buho"
msgstr "Buho"

#: src/main.cpp:55
#, kde-format
msgid "Create and organize your notes."
msgstr "Створення та упорядковування нотаток."

#: src/main.cpp:55
#, kde-format
msgid "© 2019-%1 Maui Development Team"
msgstr "© Команда розробників Maui, 2019–%1"

#: src/main.cpp:56
#, kde-format
msgid "Camilo Higuita"
msgstr "Camilo Higuita"

#: src/main.cpp:56
#, kde-format
msgid "Developer"
msgstr "Розробник"

#: src/views/notes/NotesView.qml:71
#, kde-format
msgid "Remove notes"
msgstr "Вилучити нотатки"

#: src/views/notes/NotesView.qml:72
#, kde-format
msgid "Are you sure you want to delete the selected notes?"
msgstr "Ви справді хочете вилучити позначені нотатки?"

#: src/views/notes/NotesView.qml:102
#, kde-format
msgid "No notes!"
msgstr "Немає нотаток!"

#: src/views/notes/NotesView.qml:103
#, kde-format
msgid "You can quickly create a new note"
msgstr "Ви можете швидко створити нотатку"

#: src/views/notes/NotesView.qml:107
#, kde-format
msgid "New note"
msgstr "Нова нотатка"

#: src/views/notes/NotesView.qml:129
#, kde-format
msgid "Search "
msgstr "Шукати "

#: src/views/notes/NotesView.qml:129
#, kde-format
msgid "notes"
msgstr "нотатки"

#: src/views/notes/NotesView.qml:143
#, kde-format
msgid "Settings"
msgstr "Параметри"

#: src/views/notes/NotesView.qml:150
#, kde-format
msgid "About"
msgstr "Про програму"

#: src/views/notes/NotesView.qml:281
#, kde-format
msgid "Favorite"
msgstr "Улюблене"

#: src/views/notes/NotesView.qml:294 src/views/notes/NotesView.qml:540
#, kde-format
msgid "Share"
msgstr "Оприлюднити"

#: src/views/notes/NotesView.qml:300 src/views/notes/NotesView.qml:520
#, kde-format
msgid "Export"
msgstr "Експортувати"

#: src/views/notes/NotesView.qml:306 src/widgets/NewNoteDialog.qml:144
#, kde-format
msgid "Delete"
msgstr "Вилучити"

#: src/views/notes/NotesView.qml:509
#, kde-format
msgid "UnFav"
msgstr "Не улюбл."

#: src/views/notes/NotesView.qml:509 src/widgets/SettingsDialog.qml:176
#, kde-format
msgid "Fav"
msgstr "Улюбл."

#: src/views/notes/NotesView.qml:530
#, kde-format
msgid "Copy"
msgstr "Копіювати"

#: src/views/notes/NotesView.qml:550
#, kde-format
msgid "Select"
msgstr "Вибрати"

#: src/views/notes/NotesView.qml:569
#, kde-format
msgid "Remove"
msgstr "Вилучити"

#: src/widgets/CardDelegate.qml:79
#, kde-format
msgid "Empty"
msgstr "Спорожнити"

#: src/widgets/CardDelegate.qml:80
#, kde-format
msgid "Edit this note"
msgstr "Редагувати цю нотатку"

#: src/widgets/NewNoteDialog.qml:134
#, kde-format
msgid "Find and Replace"
msgstr "Знайти і замінити"

#: src/widgets/NewNoteDialog.qml:185
#, kde-format
msgid ""
"Title\n"
"Body"
msgstr ""
"Заголовок\n"
"Вміст"

#: src/widgets/NewNoteDialog.qml:209
#, kde-format
msgid "Note saved"
msgstr "Нотатку збережено"

#: src/widgets/SettingsDialog.qml:13
#, kde-format
msgid "Editor"
msgstr "Редактор"

#: src/widgets/SettingsDialog.qml:14
#, kde-format
msgid "Configure the editor behaviour."
msgstr "Налаштувати поведінку редактора."

#: src/widgets/SettingsDialog.qml:18
#, kde-format
msgid "Spell Checker"
msgstr "Перевірка правопису"

#: src/widgets/SettingsDialog.qml:19
#, kde-format
msgid "Check spelling and give suggestions."
msgstr "Перевірка правопису та надання пропозицій щодо правильного написання."

#: src/widgets/SettingsDialog.qml:30
#, kde-format
msgid "Auto Save"
msgstr "Автозбереження"

#: src/widgets/SettingsDialog.qml:31
#, kde-format
msgid "Auto saves your file every few seconds"
msgstr "Автоматично зберігає ваш файл кожні декілька секунд"

#: src/widgets/SettingsDialog.qml:42
#, kde-format
msgid "Auto Reload"
msgstr "Автоперезавантаження"

#: src/widgets/SettingsDialog.qml:43
#, kde-format
msgid "Auto reload the text on external changes."
msgstr ""
"Автоматично перезавантажувати текст, якщо до нього внесено зміни сторонньою "
"програмою."

#: src/widgets/SettingsDialog.qml:54
#, kde-format
msgid "Line Numbers"
msgstr "Номери рядків"

#: src/widgets/SettingsDialog.qml:55
#, kde-format
msgid "Display the line numbers on the left side."
msgstr "Показувати номери рядків ліворуч."

#: src/widgets/SettingsDialog.qml:68
#, kde-format
msgid "Dark Mode"
msgstr "Темний режим"

#: src/widgets/SettingsDialog.qml:69
#, kde-format
msgid "Switch between light and dark colorscheme"
msgstr "Перемкнутися між світлою і темною схемою кольорів"

#: src/widgets/SettingsDialog.qml:86
#, kde-format
msgid "Fonts"
msgstr "Шрифти"

#: src/widgets/SettingsDialog.qml:87
#, kde-format
msgid "Configure the global editor font family and size"
msgstr "Налаштувати гарнітуру і розмір шрифту загального редактора"

#: src/widgets/SettingsDialog.qml:91
#, kde-format
msgid "Family"
msgstr "Гарнітура"

#: src/widgets/SettingsDialog.qml:104
#, kde-format
msgid "Size"
msgstr "Розмір"

#: src/widgets/SettingsDialog.qml:117
#, kde-format
msgid "Syncing"
msgstr "Синхронізація"

#: src/widgets/SettingsDialog.qml:118
#, kde-format
msgid "Configure the syncing of notes and books."
msgstr "Налаштувати синхронізацію нотаток і книг."

#: src/widgets/SettingsDialog.qml:122
#, kde-format
msgid "Auto sync"
msgstr "Автосинхронізація"

#: src/widgets/SettingsDialog.qml:123
#, kde-format
msgid "Sync notes and books on start up"
msgstr "Синхронізувати нотатки і книги під час запуску"

#: src/widgets/SettingsDialog.qml:136
#, kde-format
msgid "Sorting"
msgstr "Впорядкування"

#: src/widgets/SettingsDialog.qml:137
#, kde-format
msgid "Sorting order and behavior."
msgstr "Порядок і поведінка при упорядкуванні."

#: src/widgets/SettingsDialog.qml:141
#, kde-format
msgid "Sorting by"
msgstr "Критерій упорядкування"

#: src/widgets/SettingsDialog.qml:142
#, kde-format
msgid "Change the sorting key."
msgstr "Змінити ключ упорядкування."

#: src/widgets/SettingsDialog.qml:164
#, kde-format
msgid "Title"
msgstr "Заголовок"

#: src/widgets/SettingsDialog.qml:170
#, kde-format
msgid "Date"
msgstr "Дата"

#: src/widgets/SettingsDialog.qml:184
#, kde-format
msgid "Sort order"
msgstr "Порядок"

#: src/widgets/SettingsDialog.qml:185
#, kde-format
msgid "Change the sorting order."
msgstr "Змінити порядок сортування."

#: src/widgets/SettingsDialog.qml:206
#, kde-format
msgid "Ascending"
msgstr "За зростанням"

#: src/widgets/SettingsDialog.qml:213
#, kde-format
msgid "Descending"
msgstr "За спаданням"

#~ msgid "Tags"
#~ msgstr "Мітки"

#~ msgid "Filter "
#~ msgstr "Фільтрувати "

#~ msgid "booklets in "
#~ msgstr "буклетів у "

#~ msgid "This book is empty!"
#~ msgstr "Ця книга є порожньою!"

#~ msgid "Start by creating a new chapter for your book"
#~ msgstr "Розпочніть зі створення глави для вашої книги"

#~ msgid "Notes in this book: "
#~ msgstr "Нотатки у цій книзі: "

#~ msgid "Save"
#~ msgstr "Зберегти"

#~ msgid "Nothing to edit!"
#~ msgstr "Нічого редагувати!"

#~ msgid "Select a chapter or create a new one"
#~ msgstr "Виберіть главу або створіть нову"

#~ msgid "New Chapter"
#~ msgstr "Нова глава"

#~ msgid "Create a new chapter for your current book. Give it a title"
#~ msgstr "Створення глави для вашої поточної книги. Надайте главі назву."

#~ msgid "tags"
#~ msgstr "мітки"

#~ msgid "There are no Tags!"
#~ msgstr "Немає міток!"

#~ msgid "You can create new tags to organize your notes"
#~ msgstr "Ви можете створювати мітки для упорядковування ваших нотаток"

#~ msgid "Books"
#~ msgstr "Книги"

#~ msgid "books"
#~ msgstr "книги"

#~ msgid "New Book"
#~ msgstr "Нова книга"

#~ msgid ""
#~ "Give a title to your new book. Your new book can contain many notes "
#~ "grouped together"
#~ msgstr ""
#~ "Надайте новій книзі назву. У вашій новій книзі можуть міститися "
#~ "упорядковані нотатки."

#~ msgid "My Book..."
#~ msgstr "Книга…"

#~ msgid "Create"
#~ msgstr "Створити"

#~ msgid "Cancel"
#~ msgstr "Скасувати"

#~ msgid "Buho allows you to take quick notes and organize notebooks."
#~ msgstr ""
#~ "За допомогою Buho ви зможете записувати короткі нотатки та упорядковувати "
#~ "нотатки у книги."

#~ msgctxt "main|"
#~ msgid "Auto Fetch on Start Up"
#~ msgstr "Автоматично отримувати при запуску"

#~ msgctxt "main|"
#~ msgid "Rich Text Formatting"
#~ msgstr "Форматований текст"

#~ msgctxt "main|"
#~ msgid "Links"
#~ msgstr "Посилання"

#~ msgctxt "main|"
#~ msgid "Configure the app plugins and behavior."
#~ msgstr "Налаштувати додатки і поведінку програми."

#~ msgctxt "main|"
#~ msgid "Cached"
#~ msgstr "Кешовано"

#~ msgctxt "main|"
#~ msgid "Support Syntax Highlighting"
#~ msgstr "Підтримка підсвічування синтаксису"

#~ msgctxt "BookletPage|"
#~ msgid "Chapters"
#~ msgstr "Глави"

#~ msgctxt "BookletPage|"
#~ msgid "New chapter"
#~ msgstr "Нова глава"

#~ msgctxt "LinksView|"
#~ msgid "links"
#~ msgstr "посилання"

#~ msgctxt "LinksView|"
#~ msgid "Add date"
#~ msgstr "Додати дату"

#~ msgctxt "LinksView|"
#~ msgid "Updated"
#~ msgstr "Оновлено"

#~ msgctxt "LinksView|"
#~ msgid "Favorite"
#~ msgstr "Улюблене"

#~ msgctxt "LinksView|"
#~ msgid "No Links!"
#~ msgstr "Немає посилань!"

#~ msgctxt "LinksView|"
#~ msgid "Click here to save a new link"
#~ msgstr "Клацніть тут, щоб зберегти нове посилання"

#~ msgctxt "LinksView|"
#~ msgid "Export"
#~ msgstr "Експортувати"

#~ msgctxt "NotesView|"
#~ msgid "List"
#~ msgstr "Список"

#~ msgctxt "NotesView|"
#~ msgid "Cards"
#~ msgstr "Карти"

#~ msgctxt "NotesView|"
#~ msgid "Search "
#~ msgstr "Шукати "

#~ msgctxt "NotesView|"
#~ msgid "Ascendant"
#~ msgstr "За зростанням"

#~ msgctxt "NotesView|"
#~ msgid "Descendant"
#~ msgstr "За спаданням"

#~ msgctxt "NotesView|"
#~ msgid "Title"
#~ msgstr "Заголовок"

#~ msgctxt "NotesView|"
#~ msgid "Color"
#~ msgstr "Колір"

#~ msgctxt "NotesView|"
#~ msgid "Add date"
#~ msgstr "Додати дату"

#~ msgctxt "NotesView|"
#~ msgid "Updated"
#~ msgstr "Оновлено"

#~ msgctxt "NotesView|"
#~ msgid "Copy"
#~ msgstr "Копіювати"

#~ msgctxt "NotesView|"
#~ msgid "Remove"
#~ msgstr "Вилучити"

#~ msgctxt "NewLinkDialog|"
#~ msgid "Title"
#~ msgstr "Заголовок"

#~ msgctxt "NewLinkDialog|"
#~ msgid "URL"
#~ msgstr "Адреса"

#~ msgctxt "NewNoteDialog|"
#~ msgid "Save"
#~ msgstr "Зберегти"
